package com.clock.testScripts;

import org.junit.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.clock.testBase.TestBase;
import com.clock.uiPages.AlarmPage;
import com.clock.uiPages.ClockMenuPages;
import com.clock.webGeneralFunctions.WebGeneralFunctions;

public class UpdateAlarm extends TestBase{
	
	public ExtentTest logger;
	
	@Test(priority=1)
	public void moveToAlarmSection()throws Exception{
		
		logger = report.createTest("Move to alarm section");
		ClockMenuPages cmp = new ClockMenuPages();
		AlarmPage ap = new AlarmPage();
		WebGeneralFunctions wgf = new WebGeneralFunctions();
		wgf.click(cmp.alarm(driver), driver, logger, "Move to alarm section");
		
		String text = wgf.getText(ap.alarmText(driver), driver, logger, "get alarm value");
		if(text.equalsIgnoreCase("Alarm")) {
			Assert.assertTrue(true);
		}else {
			Assert.assertTrue(false);
		}
		Thread.sleep(2000);
	}
	
	@Test(priority=2)
	public void clickCreateAlarmButton()throws Exception{
		
		logger = report.createTest("click create alaram button");
		
		WebGeneralFunctions wgf = new WebGeneralFunctions();
		AlarmPage ap = new AlarmPage();
		wgf.click(ap.createAlarmButton(driver), driver, logger, "Click create alarm button");
		Thread.sleep(2000);
	}
	
	@Test(priority=3)
	public void clickCreateAlarmCancelButton()throws Exception{
		
		logger = report.createTest("click create alaram cancel button");
		
		WebGeneralFunctions wgf = new WebGeneralFunctions();
		AlarmPage ap = new AlarmPage();
		wgf.click(ap.alarmLabelCancelButton(driver), driver, logger, "Click create alarm cancel button");
		Thread.sleep(2000);
	}
	
	@Test(priority=4)
	public void clickOnOffAlarmButton()throws Exception{
		
		logger = report.createTest("click on off button");
		
		WebGeneralFunctions wgf = new WebGeneralFunctions();
		AlarmPage ap = new AlarmPage();
		wgf.click(ap.alarmOnOffButton(driver), driver, logger, "Click on off button");
		Thread.sleep(2000);
	}
	
	@Test(priority=5)
	public void expandTheAlarm()throws Exception{
		
		logger = report.createTest("Expand the alarm setting");
		
		WebGeneralFunctions wgf = new WebGeneralFunctions();
		AlarmPage ap = new AlarmPage();
		wgf.click(ap.alarmArrowButton(driver), driver, logger, "Expand the alarm setting");
		Thread.sleep(2000);
	}
	
	

}
