package com.clock.uiPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ClockMenuPages {
	
	public WebElement alarm(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("com.google.android.deskclock:id/tab_menu_alarm"));
		return element;
	}
	
	public WebElement clock(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("com.google.android.deskclock:id/tab_menu_clock"));
		return element;
	}
	
	public WebElement timer(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("com.google.android.deskclock:id/tab_menu_timer"));
		return element;
	}
	
	public WebElement stopWatch(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("com.google.android.deskclock:id/tab_menu_stopwatch"));
		return element;
	}
	
	public WebElement bedTime(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("com.google.android.deskclock:id/tab_menu_bedtime"));
		return element;
	}

}
