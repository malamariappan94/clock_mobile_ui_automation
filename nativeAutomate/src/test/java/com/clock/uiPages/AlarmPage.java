package com.clock.uiPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AlarmPage {
	
	public WebElement createAlarmButton(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("com.google.android.deskclock:id/fab"));
		return element;
	}
	
	public WebElement alarmOnOffButton(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("com.google.android.deskclock:id/onoff"));
		return element;
	}
	
	
	public WebElement alarmArrowButton(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("com.google.android.deskclock:id/arrow"));
		return element;
	}
	
	
	public WebElement alarmRepeatButton(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("com.google.android.deskclock:id/repeat_onoff"));
		return element;
	}
	
	
	public WebElement alarmVibrateButton(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("com.google.android.deskclock:id/vibrate_onoff"));
		return element;
	}
	
	public WebElement alarmEditLabelButton(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("com.google.android.deskclock:id/edit_label"));
		return element;
	}
	
	public WebElement alarmLabelTextField(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("com.google.android.deskclock:id/textinput_placeholder"));
		return element;
	}
	
	
	public WebElement alarmLabelOkButton(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("android:id/button1"));
		return element;
	}
	
	public WebElement alarmLabelCancelButton(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("android:id/button2"));
		return element;
	}
		
	public WebElement alarmText(WebDriver driver)throws Exception{
		WebElement element = driver.findElement(By.id("com.google.android.deskclock:id/action_bar_title"));
		return element;
	}
	
}
