package com.clock.generalFunctions;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

public class generalFunctions {
	
	public int genrateRandomNumber()throws Exception{
		
		Random r = new Random();
		int number = r.nextInt(10);
		return number;
	}
	
	public String generateRandomAlphabet()throws Exception{
		
		String value = RandomStringUtils.randomAlphabetic(10);
		return value;
	}

	public String generateRandomAlphaNumeric()throws Exception{
		
		String value = RandomStringUtils.randomAlphanumeric(10);
		return value;
	}

}
