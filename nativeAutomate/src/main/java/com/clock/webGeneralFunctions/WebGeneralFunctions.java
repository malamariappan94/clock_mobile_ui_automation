package com.clock.webGeneralFunctions;

import java.awt.Color;
import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class WebGeneralFunctions {
	
	public static String screenShotPath = System.getProperty("user.dir");
	public static Integer failure = 1;
	
	public static void click(WebElement element,WebDriver driver,ExtentTest logger,String message) throws Exception {
		try {
			element.click();
		}catch(Exception e) {
			screenShotPath += "/screenshots/failure_"+failure.toString()+".jpg"; 
			failure++;
			WebGeneralFunctions.screenShot(driver);
			logger.log(Status.FAIL, MarkupHelper.createLabel(message, ExtentColor.RED));
		}
	}
	
	
	public static void sendKeys(String value,WebElement element,WebDriver driver,ExtentTest logger,String message) throws Exception {
		try {
			element.sendKeys(value);
		}catch(Exception e) {
			screenShotPath += "/screenshots/failure_"+failure.toString()+".jpg"; 
			failure++;
			WebGeneralFunctions.screenShot(driver);
			logger.log(Status.FAIL, MarkupHelper.createLabel(message, ExtentColor.RED));
		}
	}
	
	public static String getText(WebElement element,WebDriver driver,ExtentTest logger,String message) throws Exception {
		String text = "";
		try {
			text = element.getText();
		}catch(Exception e) {
			screenShotPath += "/screenshots/failure_"+failure.toString()+".jpg"; 
			failure++;
			WebGeneralFunctions.screenShot(driver);
			logger.log(Status.FAIL, MarkupHelper.createLabel(message, ExtentColor.RED));
		}
		return text;
	}
	
	
	public static void screenShot(WebDriver driver)throws Exception{
		
		TakesScreenshot shot = ((TakesScreenshot)driver);
		File srcFile = shot.getScreenshotAs(OutputType.FILE);
		File destination = new File(screenShotPath);
		FileUtils.copyFile(srcFile, destination);
		
	}

}
