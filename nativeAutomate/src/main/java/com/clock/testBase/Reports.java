package com.clock.testBase;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports extends TestBase {

	
	//@BeforeTest
	public void startReports() throws Exception{
		
		String path = System.getProperty("user.dir")+"/test_output/results.html";
		htmlReporter = new ExtentHtmlReporter(path);
		report = new ExtentReports();
		report.attachReporter(htmlReporter);
	}
	
	//@AfterTest
	public void flushReports() throws Exception{
		report.flush();
	}
	
}
